package com.thebmw.minecraft.mods.thebmwtech;

import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;


@Mod(modid = ThebmwTech.MODID, version = ThebmwTech.VERSION)
public class ThebmwTech
{
    public static final String MODID = "thebmwtech";
    public static final String VERSION = "0.1-dev";
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        // some example code
        System.out.println("DIRT BLOCK >> "+Blocks.DIRT.getUnlocalizedName());
    }
}
